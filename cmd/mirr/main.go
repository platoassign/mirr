package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/driver-devel/mirr/internal/app"
	"github.com/driver-devel/mirr/internal/config"
)

func main() {
	cfg, err := config.FromEnv()

	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "%s\n", err.Error())
		os.Exit(-1)
	}

	ctx, cancel := context.WithCancel(context.Background())

	stoppedCh := make(chan struct{})
	sigCh := make(chan os.Signal)
	signal.Notify(sigCh, os.Interrupt, syscall.SIGTERM)

	go func() {
		select {
		case sig := <-sigCh:
			_, _ = fmt.Fprintf(os.Stderr, "Received [%s]. Terminating...\n", sig)
			cancel()

			select {
			case <-stoppedCh:
			case <-time.After(time.Second * 2):
				os.Exit(-1)
			}
		case <-stoppedCh:
		}
	}()

	app.Run(ctx, *cfg)
	stoppedCh <- struct{}{}
}
