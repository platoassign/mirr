module github.com/driver-devel/mirr

go 1.16

require (
	github.com/stretchr/testify v1.7.0
	golang.org/x/net v0.0.0-20210119194325-5f4716e94777
)
