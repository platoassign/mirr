package app

import (
	"context"
	"fmt"
	"net/http"
	"os"

	"github.com/driver-devel/mirr/internal/config"
)

type app struct {
	ctx context.Context
	cfg config.Config
}

func Run(ctx context.Context, cfg config.Config) {
	app := &app{ctx: ctx, cfg: cfg}
	app.run()
}

func (app *app) run() {
	fetcher := newHttpFetcher(http.DefaultClient)
	store := newFsStore(app.cfg.Directory)
	walker := newWalker(app.ctx, app.cfg.RootURL.String(), app.cfg.Parallel, fetcher, store)
	walker.Run()
}

func printError(err error) {
	_, _ = fmt.Fprintf(os.Stderr, "%+v", err)
}
