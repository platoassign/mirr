package app

import (
	"context"
	"io"
	"net/http"
)

var _ fetcher = (*httpFetcher)(nil)

type fetcher interface {
	Fetch(ctx context.Context, url string) (io.ReadCloser, error)
}

type httpFetcher struct {
	client *http.Client
}

func newHttpFetcher(client *http.Client) *httpFetcher {
	return &httpFetcher{client}
}

func (f *httpFetcher) Fetch(ctx context.Context, url string) (io.ReadCloser, error) {
	req, err := http.NewRequestWithContext(ctx, "GET", url, nil)

	if err != nil {
		return nil, err
	}

	resp, err := f.client.Do(req)

	if err != nil {
		return nil, err
	}

	return resp.Body, nil
}
