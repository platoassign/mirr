package app

import (
	"io"

	"golang.org/x/net/html"
)

func getAllLinks(r io.Reader) ([]string, error) {
	tokenizer := html.NewTokenizer(r)
	var links []string
	var uniqLinks = make(map[string]struct{})

loop:
	for {
		tknType := tokenizer.Next()

		switch {
		case tknType == html.ErrorToken:
			break loop

		case tknType == html.StartTagToken || tknType == html.SelfClosingTagToken:
			t := tokenizer.Token()

			if t.Data != "a" {
				continue
			}

			for _, a := range t.Attr {
				if a.Key == "href" {
					uniqLinks[a.Val] = struct{}{}
				}
			}
		}
	}

	if tokenizer.Err() != io.EOF {
		return links, tokenizer.Err()
	}

	for link, _ := range uniqLinks {
		links = append(links, link)
	}

	return links, nil
}
