package app

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_getAllLinks(t *testing.T) {
	html := `
<body>
	<a href="http://link1">l1</a>
	<a href="http://link1" />
	<a href="http://link2" />
</body>
`

	links, err := getAllLinks(bytes.NewBufferString(html))
	assert.NoError(t, err)
	assert.Len(t, links, 2)
}
