package app

import (
	"container/list"
	"sync"
)

type urlQueueItem struct {
	url    string
	refUrl string
	depth  int
}

type urlQueue struct {
	ql      *list.List
	m       *sync.Mutex
	rootUrl string
	visited map[string]struct{}
}

func newUrlQueue(rootUrl string) *urlQueue {
	urlQueue := &urlQueue{
		m:       &sync.Mutex{},
		ql:      list.New(),
		rootUrl: rootUrl,
		visited: make(map[string]struct{}),
	}

	urlQueue.ql.PushBack(&urlQueueItem{
		url:    rootUrl,
		refUrl: "",
		depth:  0,
	})

	return urlQueue
}

func (q *urlQueue) Len() int {
	q.m.Lock()
	defer q.m.Unlock()
	return q.ql.Len()
}

func (q *urlQueue) AnalyzeAndPushLink(ref string, link string, depth int) bool {
	if !isChildURL(q.rootUrl, link) {
		return false
	}

	q.m.Lock()
	defer q.m.Unlock()

	if _, ok := q.visited[link]; !ok {
		q.ql.PushBack(&urlQueueItem{
			url:    link,
			refUrl: ref,
			depth:  depth,
		})

		q.visited[link] = struct{}{}
		return true
	}

	return false
}

func (q *urlQueue) Pop() (*urlQueueItem, bool) {
	q.m.Lock()
	defer q.m.Unlock()

	el := q.ql.Front()

	if el == nil {
		return nil, false
	}

	q.ql.Remove(el)

	return el.Value.(*urlQueueItem), true
}
