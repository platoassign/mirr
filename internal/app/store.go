package app

import (
	"io"
)

var _ store = (*fsStore)(nil)

type store interface {
	Store(link string, r io.Reader) error
}

type fsStore struct {
	rootDir string
}

func newFsStore(rootDir string) *fsStore {
	return &fsStore{rootDir: rootDir}
}

func (f fsStore) Store(link string, reader io.Reader) error {
	// TODO: store content at the path $rootDir/$linkPath/index.html
	return nil
}
