package app

import (
	"fmt"
	"regexp"
)

func isChildURL(parentURL string, childURL string) bool {
	quotedRegexString := fmt.Sprintf(`%s/.`, regexp.QuoteMeta(parentURL))
	rgx := regexp.MustCompile(quotedRegexString)
	return rgx.MatchString(childURL)
}
