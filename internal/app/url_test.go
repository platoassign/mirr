package app

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_isChildURL(t *testing.T) {
	testCases := []struct {
		parent   string
		child    string
		expected bool
	}{
		{"http://start.url/1", "http://start.url/123", false},
		{"http://start.url/1", "http://start.url/1/2", true},
		{"http://start.url/1", "http://start.url/1", false},
		{"http://start.url/1", "http://start.url/2", false},
		{"http://start.url/1/2/3/4", "http://start.url/1/2/3", false},
		{"http://start.url/1", "http://start1.url/1/2", false},
		{"http://start.url/1", "https://start.url/1/2", false},
	}

	for _, tc := range testCases {
		actual := isChildURL(tc.parent, tc.child)

		assert.Equal(t, tc.expected, actual,
			"Expected isChildURL for parent=`%s`, child=`%s` to be `%v`",
			tc.parent,
			tc.child,
			tc.expected,
		)
	}
}
