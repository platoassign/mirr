package app

import (
	"bytes"
	"context"
	"io/ioutil"
	"sync"
)

type walker struct {
	queue           *urlQueue
	parallelLimiter chan struct{}
	wg              *sync.WaitGroup
	ctx             context.Context
	fetcher         fetcher
	store           store
}

func newWalker(ctx context.Context, rootUrl string, maxParallel uint, f fetcher, s store) *walker {
	queue := newUrlQueue(rootUrl)

	return &walker{
		queue:           queue,
		parallelLimiter: make(chan struct{}, maxParallel),
		ctx:             ctx,
		fetcher:         f,
		wg:              &sync.WaitGroup{},
		store:           s,
	}
}

func (w *walker) Run() {
	w.wg.Add(1)
	w.parallelLimiter <- struct{}{}
	go w.handleQueue()
	w.wg.Wait()
}

func (w *walker) handleQueue() {
	defer w.wg.Done()

	item, ok := w.queue.Pop()

	if !ok {
		return
	}

	if err := w.handleQueueItem(item); err != nil {
		printError(err)
	}

	w.spawnNext()
}

func (w *walker) handleQueueItem(item *urlQueueItem) error {
	rc, err := w.fetcher.Fetch(w.ctx, item.url)

	if err != nil {
		return err
	}

	defer func() { _ = rc.Close() }()

	body, err := ioutil.ReadAll(rc)

	links, err := getAllLinks(bytes.NewBuffer(body))

	if err != nil {
		return err
	}

	if err := w.store.Store(item.url, bytes.NewBuffer(body)); err != nil {
		return err
	}

	for _, link := range links {
		w.queue.AnalyzeAndPushLink(item.url, link, item.depth+1)
	}

	return nil
}

func (w *walker) spawnNext() {
	<-w.parallelLimiter

	if w.queue.Len() == 0 {
		return
	}

	for {
		select {
		case <-w.ctx.Done():
			break
		case w.parallelLimiter <- struct{}{}:
			w.wg.Add(1)
			go w.handleQueue()
		default:
			return
		}
	}
}
