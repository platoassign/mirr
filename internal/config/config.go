package config

import (
	"fmt"
	"net/url"
	"os"
)

type Config struct {
	RootURL   *url.URL
	Directory string
	Parallel  uint
}

func FromEnv() (*Config, error) {
	if len(os.Args) < 3 {
		return nil, fmt.Errorf("you must specify url as the first argument and output directory as the second")
	}

	u, err := url.Parse(os.Args[1])

	if err != nil {
		return nil, fmt.Errorf("invalid url specified: %w", err)
	}

	dir := os.Args[2]

	stat, err := os.Stat(dir)

	if err != nil {
		return nil, err
	}

	if !stat.IsDir() {
		return nil, fmt.Errorf("%s is not a directory", dir)
	}

	return &Config{RootURL: u, Directory: dir, Parallel: 4}, nil
}
